/**
 * View Models used by Spring MVC REST controllers.
 */
package com.company.productms.web.rest.vm;
